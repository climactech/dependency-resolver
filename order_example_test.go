package resolver_test

import (
	"fmt"
	"slices"
	"strings"

	dependencyresolver "gitlab.com/climactech/dependency-resolver"
)

func Example() {
	resolver := dependencyresolver.NewEngine()
	stage := 0

	// Express the item connections. Here, we will use this sample tree:
	// (A depends on C and D, C depends on G, D depends on E, etc.)
	//
	//    |-> C -> G -|
	// A -|        |  |
	//    |-> D -| |  |
	//           | |  |
	//    |-> E <--|  |
	// B -|           |
	//    |-> F <-----|
	//
	resolver.AddDependency("A", "C")
	resolver.AddDependency("A", "D")
	resolver.AddDependency("B", "E")
	resolver.AddDependency("B", "F")
	resolver.AddDependency("C", "G")
	resolver.AddDependency("D", "E")
	resolver.AddDependency("E", "G")
	resolver.AddDependency("G", "F")

	// Notice that "F" has no dependencies,  so we did not add a call for it

	// Now, order the items in the order in which they should be processed

	orderResult := resolver.Order()

	for items := range orderResult.Items() {
		if stage > 0 {
			fmt.Print(" -> ")
		}

		slices.Sort(items)
		fmt.Print(strings.Join(items, ", "))
		stage++
	}

	fmt.Print("\n")

	// Output: F -> G -> C, E -> B, D -> A
}
