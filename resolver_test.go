package resolver_test

import (
	"context"
	"errors"
	"fmt"
	"slices"
	"strings"
	"testing"
	"time"

	"github.com/cucumber/godog"
	dependencyresolver "gitlab.com/climactech/dependency-resolver"
)

type (
	addErrCtxKey      struct{}
	addResultCtxKey   struct{}
	itemsCtxKey       struct{}
	orderResultCtxKey struct{}
	resolverCtxKey    struct{}
)

func iPerformDependencyResolution(ctx context.Context) (context.Context, error) {
	resolver := ctx.Value(resolverCtxKey{}).(*dependencyresolver.Engine)
	result := resolver.Order()
	returnCtx := context.WithValue(ctx, orderResultCtxKey{}, result)

	return returnCtx, nil
}

func itShouldDetectTheFollowingCircularDependency(ctx context.Context, body *godog.DocString) error {
	addErr := ctx.Value(addErrCtxKey{}).(error)
	addResult := ctx.Value(addResultCtxKey{}).(dependencyresolver.AddResult)
	lines := strings.Split(body.Content, "\n")
	expectedPath := strings.Split(lines[0], ",")

	if addErr == nil {
		return errors.New("the AddDependency call did not return an error on a circular dependency")
	}

	if !slices.Equal(addResult.CircularDependencyPath, expectedPath) {
		return fmt.Errorf(
			"circular dependency paths did not match (got %#v, expected %#v)",
			addResult.CircularDependencyPath, expectedPath,
		)
	}

	return nil
}

func theDependenciesShouldBeOrderedAs(ctx context.Context, body *godog.DocString) error {
	index := 0
	orderResult := ctx.Value(orderResultCtxKey{}).(*dependencyresolver.OrderResult)
	lines := strings.Split(body.Content, "\n")
	lineCount := len(lines)

	for itemsRow := range orderResult.Items() {
		lineItems := strings.Split(lines[index], ",")

		slices.Sort(lineItems)
		slices.Sort(itemsRow)

		if !slices.Equal(itemsRow, lineItems) {
			return errors.New("dependencies did not match")
		}

		index++
	}

	stageCount := index

	if lineCount != stageCount {
		return fmt.Errorf("number of dependency stages did not match (expected %d, got %d)", lineCount, stageCount)
	}

	if orderResult.Err() != nil {
		return fmt.Errorf("unexpected error: %w", orderResult.Err())
	}

	return nil
}

func theFollowingItems(ctx context.Context, items *godog.Table) (context.Context, error) {
	columns := make([]string, 0, len(items.Rows[0].Cells))
	resolver := dependencyresolver.NewEngine()
	returnCtx := context.WithValue(ctx, resolverCtxKey{}, resolver)

	for _, column := range items.Rows[0].Cells {
		columns = append(columns, column.Value)
	}

rowLoop:
	for _, row := range items.Rows[1:] {
		var (
			itemDeps []string
			itemName string
		)

		for cellIndex, cell := range row.Cells {
			switch columns[cellIndex] {
			case "dependencies":
				if cellValue := strings.TrimSpace(cell.Value); cellValue != "" {
					itemDeps = strings.Split(cellValue, ",")
				}
			case "item":
				itemName = cell.Value
			}
		}

		for _, itemDep := range itemDeps {
			if addResult, addErr := resolver.AddDependency(itemName, itemDep); addErr != nil {
				returnCtx = context.WithValue(returnCtx, addErrCtxKey{}, addErr)
				returnCtx = context.WithValue(returnCtx, addResultCtxKey{}, addResult)

				break rowLoop
			}
		}
	}

	return returnCtx, nil
}

func InitializeScenario(sc *godog.ScenarioContext) {
	sc.Given(`^the following items:$`, theFollowingItems)
	sc.When(`^I perform dependency resolution$`, iPerformDependencyResolution)
	sc.Then(`^it should detect the following circular dependency:`, itShouldDetectTheFollowingCircularDependency)
	sc.Then(`^the dependencies should be ordered as:$`, theDependenciesShouldBeOrderedAs)
}

// func TestMain(m *testing.M) {
func TestFeatures(t *testing.T) {
	opts := godog.Options{
		Format:    "progress",
		Paths:     []string{"features"},
		Randomize: time.Now().UTC().UnixNano(),
	}
	suite := godog.TestSuite{
		Name:                "godogs",
		ScenarioInitializer: InitializeScenario,
		Options:             &opts,
	}

	//os.Exit(status)
	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}
