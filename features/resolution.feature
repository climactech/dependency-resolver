Feature: Dependency Resolution
  Provided a list of items and their dependencies, we can determine which items
  need to be handled first, and during each iteration, which can be processed in
  parallel.

  Scenario: Should Order Dependencies
    Given the following items:
      | item | dependencies |
      | A    | C,D          |
      | B    | E,F          |
      | C    | G            |
      | D    | E            |
      | E    | G            |
      | F    |              |
      | G    | F            |
    When I perform dependency resolution
    Then the dependencies should be ordered as:
      """
      F
      G
      C,E
      B,D
      A
      """

  Scenario: Should Detect Circular Dependencies
    Given the following items:
      | item | dependencies |
      | A    | B,C          |
      | C    | D,E          |
      | D    | A            |
      | E    |              |
    When I perform dependency resolution
    Then it should detect the following circular dependency:
      """
      D,A,C,D
      """
