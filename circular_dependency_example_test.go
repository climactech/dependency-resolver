package resolver_test

import (
	"fmt"
	"strings"

	dependencyresolver "gitlab.com/climactech/dependency-resolver"
)

func Example_circularDependencyDetection() {
	var (
		addResult dependencyresolver.AddResult
		err       error
	)

	sampleItems := [][]string{
		{"A", "C"},
		{"A", "D"},
		{"B", "E"},
		{"B", "F"},
		{"C", "G"},
		{"D", "E"},
		{"E", "G"},
		{"F", "A"},
		{"G", "F"}, // this creates a circular dependency!
	}

	resolver := dependencyresolver.NewEngine()

	for _, connection := range sampleItems {
		if addResult, err = resolver.AddDependency(connection[0], connection[1]); err != nil {
			fmt.Printf(
				"Found circular dependency when specifying '%s' as a dependency of '%s'\n",
				connection[1], connection[0],
			)

			// You can find the path of the circular dependency using
			// CircularDependencyPath. This path is nondeterministic.
			if false {
				fmt.Println(strings.Join(addResult.CircularDependencyPath, " --> "))
			}

			break
		}
	}

	// Output: Found circular dependency when specifying 'F' as a dependency of 'G'
}
