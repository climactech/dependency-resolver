package resolver

import (
	"errors"
	"maps"

	cmap "github.com/orcaman/concurrent-map/v2"
	expmaps "golang.org/x/exp/maps"
)

var ErrCircularDependency = errors.New("encountered circular dependency")

type item struct {
	children    cmap.ConcurrentMap[string, struct{}]
	descendants cmap.ConcurrentMap[string, struct{}]
	parents     cmap.ConcurrentMap[string, struct{}]
}

// AddResult contains the results of an AddDependency call.
type AddResult struct {
	// CircularDependencyPath contains the path of a circular dependency path.
	// The first and last items will be the same.
	CircularDependencyPath []string
}

// OrderResult contains the results of an Order call.
type OrderResult struct {
	itemsCh chan []string
	lastErr error
}

// Err returns the error, if any, that was encountered during iteration.
func (o *OrderResult) Err() error {
	return o.lastErr
}

// Items contains the items sorted in the order in which they should be
// processed.
//
// Each main row can be thought of as the processing stage, and within each
// stage will be the items that can be processed, either serially or in
// parallel.
func (o *OrderResult) Items() <-chan []string {
	return o.itemsCh
}

// Engine is the dependency resolution engine.
type Engine struct {
	items cmap.ConcurrentMap[string, *item]
}

// AddDependency indicates that `dependency` is a dependency of `parent`.
func (e *Engine) AddDependency(parent string, dependency string) (AddResult, error) {
	var (
		err    error
		result AddResult
	)

	item := e.getItem(parent)
	dependentItem := e.getItem(dependency)

	item.children.Set(dependency, struct{}{})
	item.descendants.Set(dependency, struct{}{})
	item.descendants.MSet(dependentItem.descendants.Items())
	dependentItem.parents.Set(parent, struct{}{})

	e.addDescendant(parent, item.parents.Items(), dependency, dependentItem)

	if dependentItem.descendants.Has(parent) {
		result.CircularDependencyPath = e.generateCircularDependencyFrom(parent)
		err = ErrCircularDependency
	}

	return result, err
}

// addDescendant marks a dependency for all the ancestors of an item.
func (e *Engine) addDescendant(
	orgParent string,
	parents map[string]struct{},
	descendant string,
	descendantItem *item,
) {
	var (
		grandparent string
		parent      *item
	)

	grandDescendants := descendantItem.descendants.Items()
	parentsBuffer := maps.Clone(parents)

	for len(parentsBuffer) > 0 {
		for parentName := range parentsBuffer {
			if parentName != orgParent {
				parent, _ = e.items.Get(parentName)

				// The descendants of this new descendant also become's the
				// parent's descendants
				parent.descendants.Set(descendant, struct{}{})
				parent.descendants.MSet(grandDescendants)

				for _, grandparent = range parent.parents.Keys() {
					parentsBuffer[grandparent] = struct{}{}
				}
			}

			delete(parentsBuffer, parentName)
		}
	}
}

func (e *Engine) getItem(name string) *item {
	var (
		ok     bool
		result *item
	)

	if result, ok = e.items.Get(name); !ok {
		e.items.SetIfAbsent(name, e.newItem())
		result, _ = e.items.Get(name)
	}

	return result
}

func (e *Engine) generateCircularDependencyFrom(itemName string) []string {
	var child *item

	item, _ := e.items.Get(itemName)
	children := item.children.Items()
	result := []string{itemName}

childrenLoop:
	for len(children) > 0 {
		for childName, _ := range children {
			child, _ = e.items.Get(childName)

			if child.children.Has(itemName) {
				// We reached the end of the circular dependency path!
				result = append(result, childName)
				break childrenLoop
			} else if child.descendants.Has(itemName) {
				result = append(result, childName)
				children = child.children.Items()
				break
			}
		}
	}

	result = append(result, itemName)

	return result
}

// Order sorts the items in the order that they should be processed.
func (e *Engine) Order() *OrderResult {
	itemCount := e.items.Count()
	itemsChannel := make(chan []string, itemCount/2)
	result := &OrderResult{
		itemsCh: itemsChannel,
	}

	go func() {
		remainingItems := e.items.Items()
		rootItemNames := expmaps.Keys(remainingItems)

		for len(rootItemNames) > 0 {
			newRootItemNames := []string{}
			orderedItemsStage := make([]string, 0, 8)

		rootItemNamesInnerLoop:
			for _, itemName := range rootItemNames {
				item, _ := e.items.Get(itemName)

				for childItem := range item.children.IterBuffered() {
					if _, ok := remainingItems[childItem.Key]; ok {
						continue rootItemNamesInnerLoop
					}
				}

				orderedItemsStage = append(orderedItemsStage, itemName)
				newRootItemNames = append(newRootItemNames, item.parents.Keys()...)
			}

			// Return this stage's items ASAP
			itemsChannel <- orderedItemsStage

			rootItemNames = newRootItemNames

			for _, stageItem := range orderedItemsStage {
				delete(remainingItems, stageItem)
			}
		}

		close(itemsChannel)
	}()

	return result
}

// newItem creates a new item.
func (e *Engine) newItem() *item {
	return &item{
		children:    cmap.New[struct{}](),
		descendants: cmap.New[struct{}](),
		parents:     cmap.New[struct{}](),
	}
}

// NewEngine creates and returns a new dependency resolution engine.
func NewEngine() *Engine {
	return &Engine{
		items: cmap.New[*item](),
	}
}
