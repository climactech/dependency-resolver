# dependency-resolver

Small library that helps in determining the order of applying any items that are
dependent upon others. It also detects when there are circular dependencies.

Common use cases:

- Given a set of computing infrastructure resources, determine the order in
  which these resources should be created/modified.
- Generating a dependency tree of a software's third-party packages.

## Example Usage

```go
import (
    dependencyresolver "gitlab.com/climactech/dependency-resolver"
)

func main() {
    resolver := dependencyresolver.NewEngine()

    // Express the item connections. Here, we will use this sample tree:
    // (A depends on C and D, C depends on G, D depends on E, etc.)
    //
    //    |-> C -> G -|
    // A -|        |  |
    //    |-> D -| |  |
    //           | |  |
    //    |-> E <--|  |
    // B -|           |
    //    |-> F <-----|    
    //
    resolver.AddDependency("A", "C")
    resolver.AddDependency("A", "D")
    resolver.AddDependency("B", "E")
    resolver.AddDependency("B", "F")
    resolver.AddDependency("C", "G")
    resolver.AddDependency("D", "E")
    resolver.AddDependency("E", "G")
    resolver.AddDependency("G", "F")

    // Notice that "F" has no dependencies,  so we did not add a call for it

    // Now, order the items in the order in which they should be processed

    orderResult := resolver.Order()

    // At this point, orderResult.Items() will have the list of processing
    // stages and items for each stage. orderResult.Items will contain the
    // following items:
    //
    // 1: F (F is the only item with no dependencies, so naturally this must be processed first)
    // 2: G
    // 3: C, E (these two can be processed either serially or in parallel at this point)
    // 4: B, D
    // 5: A (A depended on everything else, so naturally it's the last item that should be processed)
}
```

## Copyright

Copyright © 2023-2024 Climactech, LLC
